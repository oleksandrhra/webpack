import $ from 'jquery';
import './style.scss';

// slayder

var slideNow = 1;
var slideCount = $('#slidewrapper').children().length;
var slideInterval = 6000;
var navBtnId = 0;
var translateWidth = 0;

$(document).ready(function() {
    var switchInterval = setInterval(nextSlide, slideInterval);

    $('#viewport').hover(function() {
        clearInterval(switchInterval);
    }, function() {
        switchInterval = setInterval(nextSlide, slideInterval);
    });

    $('#next-btn').click(function() {
        nextSlide();
    });

    $('#prev-btn').click(function() {
        prevSlide();
    });

    $('.slide-nav-btn').click(function() {
        navBtnId = $(this).index();

        if (navBtnId + 1 != slideNow) {
            translateWidth = -$('#viewport').width() * (navBtnId);
            $('#slidewrapper').css({
                'transform': 'translate(' + translateWidth + 'px, 0)',
                '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
                '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
            });
            slideNow = navBtnId + 1;
        }
    });
});


function nextSlide() {
    if (slideNow == slideCount || slideNow <= 0 || slideNow > slideCount) {
        $('#slidewrapper').css('transform', 'translate(0, 0)');
        slideNow = 1;
    } else {
        translateWidth = -$('#viewport').width() * (slideNow);
        $('#slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow++;
    }
}

function prevSlide() {
    if (slideNow == 1 || slideNow <= 0 || slideNow > slideCount) {
        translateWidth = -$('#viewport').width() * (slideCount - 1);
        $('#slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow = slideCount;
    } else {
        translateWidth = -$('#viewport').width() * (slideNow - 2);
        $('#slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow--;
    }
}


function idi() {

}


// media

var elem = document.querySelector(".music");

$(window).ready(function(){
    if(document.documentElement.clientWidth < 1200) {
        document.getElementById('menu1').className = 'display1';
        document.getElementById('menu2').className = 'display0';
        document.getElementById('menu3').className = 'display0';

    }
    if(document.documentElement.clientWidth > 1052) {
        document.getElementById('block-for-slider').className = 'display1';
        document.getElementById('backgraund').className = 'display0';
        elem.classList.add('absolute');
    }
    if(document.documentElement.clientWidth < 1051) {
        document.getElementById('menu1').className = 'display1';
        document.getElementById('menu2').className = 'display1';
        document.getElementById('menu3').className = 'display0';
        document.getElementById('block-for-slider').className = 'display0';
        document.getElementById('backgraund').className = 'display1';
        elem.classList.remove('absolute');
    }

    if(document.documentElement.clientWidth < 800) {
        document.getElementById('menu1').className = 'display1';
        document.getElementById('menu2').className = 'display1';
        document.getElementById('menu3').className = 'display1';


    }
});

$(window).resize(function() {
    if(document.documentElement.clientWidth > 1200) {
        document.getElementById('menu1').className = 'display0';
        document.getElementById('menu2').className = 'display0';
        document.getElementById('menu3').className = 'display0';

    }
    if(document.documentElement.clientWidth < 1200) {
        document.getElementById('menu1').className = 'display1';
        document.getElementById('menu2').className = 'display0';
        document.getElementById('menu3').className = 'display0';

    }
    if(document.documentElement.clientWidth > 1052) {
        document.getElementById('block-for-slider').className = 'display1';
        document.getElementById('backgraund').className = 'display0';

        elem.classList.add('absolute');

    }
    if(document.documentElement.clientWidth < 1051) {
        document.getElementById('menu1').className = 'display1';
        document.getElementById('menu2').className = 'display1';
        document.getElementById('menu3').className = 'display0';
        document.getElementById('block-for-slider').className = 'display0';
        document.getElementById('backgraund').className = 'display1';
        elem.classList.remove('absolute');

    }
    if(document.documentElement.clientWidth < 800) {
        document.getElementById('menu1').className = 'display1';
        document.getElementById('menu2').className = 'display1';
        document.getElementById('menu3').className = 'display1';
    }

    });

//player
$(document).ready(function() {
    $('#audio-player').mediaelementplayer({
        alwaysShowControls: true,
        features: ['playpause','volume','progress'],
        audioVolume: 'horizontal',
        audioWidth: 400,
        audioHeight: 120
    });
});



